<?php
require_once("config.php");
session_start();
$id_servico = filter_input(INPUT_GET, "servico_id", FILTER_SANITIZE_NUMBER_INT);
if(isset($id_servico)){
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $statement = $conexao->prepare("SELECT servico.servico_id, servico.data_criada, servico.tipo_serv_id, servico.prestador_id, servico.data_conclusao, tipo_servico.nome_servico, usuarios.email FROM servico JOIN usuarios ON usuarios.usuario_id = servico.prestador_id JOIN tipo_servico ON tipo_servico.tipo_serv_id = servico.servico_id WHERE servico_id = :id");
   $statement->bindParam(":id", $id_servico);
   $statement->execute();
   $servico = $statement->fetch(PDO::FETCH_ASSOC);
   if(!$result){
      echo ("Erro ao buscar prestador do serviço.");
   }else{
      return $result;
   }
}
?>
<html>
   <?php include("inc/heading.html"); ?>
   <body>
      <div class="container">
         <?php include("inc/avatar.inc"); ?>
         
         <h4>Avalie seu prestador</h4>
         <p>Serviço: <?= servico["nome_servico"] ?></p>
         <p>Data: <?= servico["data_criada"] ?></p>
         <p>Data Conclusão: <?= servico["data_conclusao"] ?></p>
         <p>Prestador: <?= servico["email"] ?></p>
         <span>Nota: 
         <input name="nota" type="radio" value="1">1
         <input name="nota" type="radio" value="2">2
         <input name="nota" type="radio" value="3">3
         <input name="nota" type="radio" value="4">4
         <input name="nota" type="radio" value="5">5         
         </span>
         <p>Comentário: </p>
         <textarea rows="4" cols="200" id="comentario" name="comentario"></textarea>
         <button id="botao_avaliar" class="btn btn-primary col-xs-12 col-sm-12 col-md-12 col-lg-12">Registrar avaliação</button>
      </div>
      <script>
      $("#botao_avaliar").on("click", function avalia(){
         var nota = $("input:checked").val();         
      });
      </script>
   </body>
</html>