<?php
session_start();
$tipocadastro = isset($_GET["linktipo"]) ? $_GET["linktipo"] : '';
$_SESSION['tipocadastro'] = $tipocadastro;
$tipocadastro == 2 ? $tipo = "Cliente" : $tipo = "Prestador";
?>
<!DOCTYPE html>
<html>
   <?php include("inc/heading.html"); ?>
   <body>
      <div class="container">
         <h2 id="titulo"><a href='<?= $_COOKIE['home'] ?>'>Serviço Fácil</a></h2>
         <?php
         if(isset($_GET['email_existente'])){
            if($_GET['email_existente'] == 1){
               include("inc/email_existente.inc");
               unset($_GET);
            }
         }
         if(isset($_GET['nao_encontrado'])){
            if($_GET['nao_encontrado'] == 1){
               include("inc/emailNaoEncontrado.inc");
               unset($_GET);
            }
         }
         ?>
         <form method="post" action="validacaoCadastro.php" class="form-horizontal">            
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 mx-auto">			
               <h4>Cadastro de <?= $tipo ?></h4>
               <div class="row">
                  <div class="form-group mx-auto col-sm-12 col-md-12 col-lg-12">                  
                     <label for="email">E-Mail:</label>
                     <input type="email" class="form-control" id="email" placeholder="Seu Email para cadastro" name="email" required>                  
                  </div>
               </div>
               <?php
   if(isset($_GET['invalido'])){
      if($_GET['invalido'] == true){
         echo("<p class='invalidacao'>As senhas não conferem. Preencha novamente.</p>");
      }
      unset($_GET);
   }
               ?>			   
               <div class="row">
                  <div class="form-group mx-auto  col-sm-12 col-md-12 col-lg-12">
                     <label for="senha">Senha:</label>
                     <input type="password" class="form-control" id="senha" placeholder="Senha desejada" name="senha" required>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group mx-auto  col-sm-12 col-md-12 col-lg-12">
                     <label for="senha">Repita a Senha:</label>
                     <input type="password" class="form-control" id="confsenha" placeholder="Redigite sua senha" name="confsenha" required>
                  </div>
               </div>
               <button type="submit" class="btn btn-primary col-xs-12 col-sm-12 col-md-12 col-lg-12">Registrar</button>
               <br><br>                                            
            </div>
         </form>
      </div>
   </body>
</html>