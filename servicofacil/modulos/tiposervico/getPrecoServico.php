<?php
require_once("../../config.php");

$id = filter_input(INPUT_GET,'id_tipoServico',FILTER_SANITIZE_NUMBER_INT);

if(isset($id)){

   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $statement = $conexao->query("SELECT preco FROM tipo_servico WHERE tipo_serv_id = $id");

   $result = $statement->fetch();      

   echo $result['preco'];
}
?>