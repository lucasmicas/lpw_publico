<?php
require_once("../../config.php");
session_start();

$id = filter_input(INPUT_POST,'idTipoServico',FILTER_SANITIZE_NUMBER_INT);

if(isset($id)){   
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

   //PRESTADORES DISPONIVEIS
   $statement = $conexao->query("SELECT u.usuario_id AS id_usuario, u.email, s.data_criada, s.data_conclusao, s.status_id, s.pendencias FROM usuarios u LEFT JOIN servico s ON s.prestador_id = u.usuario_id WHERE u.tipo_id = 1 AND u.usuario_id NOT IN (SELECT prestador_id FROM servico WHERE status_id = 1 OR status_id = 5)");
   $prestadores = $statement->fetchAll(PDO::FETCH_ASSOC);
   if(count($prestadores) < 1){
      echo "-1";
   }else{
      $numeroPrestadores = count($prestadores);
      //CONTAGEM DE PENDENCIAS DE PRESTADORES COM SERVICOS JA FEITOS
      $statement = $conexao->query("SELECT prestador_id, COUNT(pendencias) AS num_pendencias FROM servico WHERE prestador_id NOT IN (SELECT prestador_id FROM servico WHERE status_id = 1 OR status_id = 5) GROUP BY prestador_id");
      $associacaoPendencias = $statement->fetchAll(PDO::FETCH_ASSOC);      
      //Cria chave valor(id,pendencias) com prestadores      
      $chaveValor = array();      
      $num_associacoes = count($associacaoPendencias);
      for($i = 0; $i < $numeroPrestadores; $i++){
         $contagem_pendencias = 0;
         for($j = 0; $j < $num_associacoes; $j++){            
            if($prestadores[$i]["id_usuario"] == $associacaoPendencias[$j]["prestador_id"]){
               $contagem_pendencias = $associacaoPendencias[$j]["num_pendencias"];
               break;
            }            
         }
         $chaveValor[$prestadores[$i]["id_usuario"]] = $contagem_pendencias;
      }

      //Calcula prestadores com menos pendencias
      $menorOuMenoresPendencias = array();      
      $totalCiclos = count($chaveValor);
      foreach($chaveValor as $id_prest => $contagem){         
         $ciclo = 0;
         foreach($chaveValor as $id_interno => $contagem_interno){
            if($contagem <= $contagem_interno && $id_prest != $id_interno){
               $menorOuMenoresPendencias[$id_prest] = $contagem;
               break;
            }
            $ciclo += 1;
            if($ciclo > $totalCiclos){
               $menorOuMenoresPendencias[$id_prest] = $contagem;
               break;
            }
         }
         if($ciclo > $totalCiclos){
            break;
         }
      }
      $qt_menoresPendencias = count($menorOuMenoresPendencias);
      $contaOr = 0;
      if($qt_menoresPendencias != 1){         
         $whereClause = "";
         foreach($menorOuMenoresPendencias as $id_prest => $contagem){
            if($contaOr == 0){
               $whereClause .= "prestador_id = " . $id_prest;
               $contaOr += 1;
            }else{
               $whereClause .= " OR prestador_id = " . $id_prest;
               $contaOr += 1;
            }            
         }               

         $queryWhere = "SELECT * FROM servico WHERE " . $whereClause . " ORDER BY data_conclusao";

         $statement = $conexao->query($queryWhere);
         $prestador = $statement->fetch(PDO::FETCH_ASSOC);
         $prestadorEscalado = $prestador["prestador_id"];
         $retorno = inserirServico( $id, $_SESSION["user_id"], $prestadorEscalado);
         echo($retorno);
      }else{
         $prestadorEscalado = key($menorOuMenoresPendencias);
         $retorno = inserirServico( $id, $_SESSION["user_id"], $prestadorEscalado);
         echo($retorno);
      }
   }   
}
//INSERE SERVICO
function inserirServico($id_tipoServico, $id_cliente, $id_prestador){
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   //Busca valor do servico e credito cliente
   $valorTotal = 0;
   $data_criacao = date("Y-m-d H:i:s");
   $statement = $conexao->prepare("SELECT preco FROM tipo_servico WHERE tipo_serv_id = :id_servico");
   $statement->bindParam(':id_servico', $id_tipoServico, PDO::PARAM_INT);
   $statement->execute();
   $resposta_valorServico = $statement->fetch(PDO::FETCH_ASSOC);
   $valorServico = $resposta_valorServico["preco"];

   //Busca saldo cliente
   $statement = $conexao->prepare("SELECT creditos FROM usuarios WHERE usuario_id = :id_cliente");
   $statement->bindParam(':id_cliente', $id_cliente, PDO::PARAM_INT);
   $statement->execute();
   $resposta_saldoCliente = $statement->fetch(PDO::FETCH_ASSOC);
   $saldoCliente = $resposta_saldoCliente["creditos"];

   if($saldoCliente <= $valorServico){
      $valorTotal = $valorServico - $saldoCliente;
      $saldoCliente = 0;
   }else{
      $valorTotal = 0;
      $saldoCliente -= $valorServico;
   }

   //Insere servico
   $statement = $conexao->prepare("INSERT INTO servico (data_criada, tipo_serv_id, status_id, usuario_id, prestador_id, valor_servico) VALUES (:id_data, :id_tipoServico, 5, :id_clienteIns, :id_prestador, :valorServico)");
   $statement->bindParam(':id_data', $data_criacao);
   $statement->bindParam(':id_tipoServico', $id_tipoServico, PDO::PARAM_INT);
   $statement->bindParam(':id_clienteIns', $id_cliente, PDO::PARAM_INT);
   $statement->bindParam(':id_prestador', $id_prestador, PDO::PARAM_INT);
   $statement->bindParam(':valorServico', $valorTotal);   
   $statement->execute();
   $retorno_id_servico = $conexao->lastInsertId();

   //Altera saldo cliente
   $statement = $conexao->prepare("UPDATE usuarios SET creditos = :creditos WHERE usuario_id = :id_clienteUp;");
   $statement->bindParam(':id_clienteUp', $id_cliente, PDO::PARAM_INT);
   $statement->bindParam(':creditos', $saldoCliente);
   $statement->execute();

   $_SESSION["creditos"] = $saldoCliente;

   //Busca email prestador
   $statement = $conexao->prepare("SELECT email FROM usuarios WHERE usuario_id = :id_prestadorBusca");
   $statement->bindParam(':id_prestadorBusca', $id_prestador, PDO::PARAM_INT);
   $statement->execute();
   $email = $statement->fetch(PDO::FETCH_ASSOC);

   $array_retorno = array();      
   $array_retorno['id_servico'] = $retorno_id_servico;
   $array_retorno['email_prestador'] = $email;
   /*$array_retorno = "{
                     id_servico:" .  $retorno_id_servico . ",
                     email_prestador:" . $email . "
                     }";*/

   return json_encode($array_retorno);
}
?>