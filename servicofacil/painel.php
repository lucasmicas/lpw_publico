<?php
session_start();
require_once("config.php");
if(isset($_SESSION['tipo'])){ // PRESTADOR 1 | CLIENTE 2
   if($_SESSION['tipo'] == 1){
      include("inc/painelPrestador.inc");
   }else{
      include("inc/painelCliente.inc");
   }
}else{
   header('location:index.php');
}
?>