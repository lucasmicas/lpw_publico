<?php
session_start();
require_once('config.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

if(isset($_POST['email']) && isset($_POST['senha']) && isset($_POST['confsenha'])){
   $email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING);
   $senha = filter_input(INPUT_POST, "senha", FILTER_SANITIZE_STRING);
   $confsenha = filter_input(INPUT_POST, "confsenha", FILTER_SANITIZE_STRING);
   $tipocadastro = $_SESSION['tipocadastro']; 
}
$senha == $confsenha ? $invalido = false : $invalido = true;

if($invalido == true){
   header('Location: cadastro.php?invalido=true');
}else{
   $senha = base64_encode($senha);
   inserirCadastro($email, $senha, $tipocadastro);
}

function inserirCadastro($email, $senha, $tipocadastro){
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   
   //Valida existência
   $statement = $conexao->query("SELECT email FROM usuarios WHERE email = '$email'");
   $result = $statement->fetch(PDO::FETCH_ASSOC);
   if($result){
      header('Location: cadastro.php?email_existente=1');
   }else{
      $statement = $conexao->query("INSERT INTO usuarios(email, senha, tipo_id) VALUES ('$email', '$senha', $tipocadastro)");      
      if($statement){
         $url = (isset($_SERVER['HTTPS']) ? "https" : "http")."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
         $url = str_replace("validacaoCadastro","validacaoConta",$url); 
         enviaEmail($email, $url);
      }else{
         echo 'Não foi possível criar seu cadastro, tente novamente mais tarde.';
      }
   }   
}

function enviaEmail($email, $url){   
   $remetente = "aula.mail.sender@gmail.com";
   $senha = "netaula321";
   $key = base64_encode($email);   
   $url = $url . '?key=' . $key;
   $mail = new PHPMailer(true);
   $mail->CharSet = 'UTF-8';
   $mail->isSMTP();
   $mail->SMTPDebug = 0;
   $mail->Host = 'smtp.gmail.com';
   $mail->Port = 587;
   $mail->SMTPSecure = 'tls';
   $mail->SMTPAuth = true;   
   $mail->Username = $remetente;
   $mail->Password = $senha;
   $mail->setFrom($remetente, 'Servico Facil');
   $mail->addAddress($email, 'Voce');
   $mail->Subject = "Seu registro no Serviço Fácil";
   $mail->isHTML(true);
   $mail->Body = "Obrigado pelo cadastro. Para validar sua conta acesse o link abaixo:<br>$url";
   if (!$mail->send()){
      echo "O email de validação não pôde ser entregue. Não foi possível completar seu cadastro.<script>console.log('$mail->ErrorInfo')</script>";
   }else{
      echo("<html>");
      include('inc/heading.html');
      echo('<body>');   
      echo("<h5>Você está cadastrado no Serviço Fácil!</h5><h6>Um email com a validação foi enviado para $email e chegará em no máximo 15 minutos.<br>Verifique sua caixa de entrada e acesse o link fornecido.<br>Certifique-se de que o email não tenha sido enviado para a caixa de SPAM.</h6>");
      echo('</body></html>');
   }
}
?>