<?php
session_start();
require_once("config.php");
$categorias = getCategorias();
?>
<html>
   <?php
   include("inc/heading.html");
   include("js/scripts.html");
   ?>
   <body>
      <div class="container">
         <?php include("inc/avatar.inc"); ?>
         <h4>Escolha o serviço desejado</h4>
         <form class="form-vertical" onsubmit="return false;">
            <div class="form-group">
               <label for="categoria">Tipo de serviço:</label>
               <select class="form-control" id="categoria" name="categoria" onchange="buscaCategoria(this.value)" required>
                  <option value="" selected disabled>Selecione uma categoria</option>
                  <?php foreach( $categorias as $categoria) { ?>
                  <option value="<?= $categoria['categoria_id'] ?>">
                     <?= $categoria['nome_categoria'] ?></option>
                  <?php } ?>
               </select>
               <br>
               <select multiple class="form-control" name="tipoServicos" id="tipoServicos" required>
               </select>
               <br>
               <button id="escolher_servico" class="btn btn-primary col-xs-12 col-sm-12 col-md-4 col-lg-4 mx-auto">Escolher</button>
               <?php
               include("inc/confirma_preco.inc");
               include("inc/confirma_solicitacao.inc");
               ?>
               <script>
                  function buscaCategoria(codigoCategoria) {
                     var requisicao;
                     if (codigoCategoria == ""){
                        document.getElementById("tipoServicos").innerHTML = "";
                        return;
                     }
                     requisicao = new XMLHttpRequest();
                     requisicao.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                           document.getElementById("tipoServicos").innerHTML = this.responseText;
                           //response -> Serviços => class='selecao_servicos'
                        }
                     };
                     requisicao.open("GET", "inc/getTiposServico.php?cat="+codigoCategoria, true);
                     requisicao.send();
                  }
                  
                  //CONFIRMA PREÇO
                  $("#escolher_servico").on("click",
                                            function confirmarPreco(){
                     var id_categoriaServico = $("#categoria option:selected").val();
                     var nome_categoriaServico = $("#categoria option:selected").text();
                     var id_tipoServico = $("#tipoServicos option:selected").val();
                     var nome_tipoServico = $("#tipoServicos option:selected").text();

                     $("#modal_categoria_servico").text(nome_categoriaServico);
                     $("#modal_tipoServico").text(nome_tipoServico);
                     $("#modal_tipoServico").val(id_tipoServico);

                     getPrecoTipoServico(id_tipoServico, calculaValores);

                     $("#confirmaPreco_trigger").click();
                  });
                  
                  //CONFIRMA SOLICITAÇÃO
                  $("#confirmaPrecoSolicitacao_trigger").on("click", function confirmarSolicitacao(){                     
                     $.post("modulos/servico/confirmarSolicitacao.php",{
                        idTipoServico: $("#modal_tipoServico").val()
                     }).done(function( data ){                                                                       
                        $("#modal_num_solicitacao").text(data["id_servico"]);
                        $("#modal_prestador_escalado").text(data["email_prestador"]);
                        $("#modal_confirmaSolicitacao_trigger").click();
                     }).fail(function(){
                        alert("Não há prestadores disponíveis no momento.");
                     });
                  });
                  <?php
                  include("js/tipo_servico.js");
                  ?>

                  function calculaValores(precoServico){
                     var precoServico = parseFloat(precoServico);
                     var creditos = parseFloat(<?= $_SESSION["creditos"] ?>);
                     if(creditos > precoServico)
                        var cobrado = parseFloat(0);
                     else
                        var cobrado = parseFloat(precoServico - creditos);

                     $("#modal_preco").text((precoServico).toFixed(2));
                     $("#modal_credito").text((creditos).toFixed(2));
                     $("#modal_cobrado").text((cobrado).toFixed(2));
                  }
               </script>
            </div> 
         </form>
      </div>      
   </body>
</html>
<?php
   function getCategorias(){
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $statement = $conexao->query("SELECT * FROM categoria");
   $result = $statement->fetchAll(PDO::FETCH_ASSOC);
   if(!$result){
      echo ("Erro ao buscar categorias. Atualizar a página pode resolver o problema.");
   }else{
      return $result;
   }
}
?>
