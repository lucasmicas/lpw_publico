<?php
   session_start();
   session_destroy();
   $inicio = (isset($_SERVER['HTTPS']) ? "https" : "http")."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
   setcookie('home', $inicio);
?>
<!DOCTYPE html>
<html>   
   <?php include("inc/heading.html"); ?>
   <body>
      <div class="container">
         <h2 id="titulo"><a href="<?= $_COOKIE['home'] ?>">Serviço Fácil</a></h2>
         <form method="POST" action="validacaoLogin.php" class="form-horizontal">
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 mx-auto">               
               <div class="row">
                  <div class="form-group mx-auto col-sm-12 col-md-12 col-lg-12">
                     <label for="email">E-Mail:</label>
                     <input type="email" class="form-control" id="email" placeholder="Seu Email" name="email" required>                  
                  </div>
               </div>
               <?php
               if(isset($_GET['invalidacao'])){
                  if($_GET['invalidacao'] == 1){
                     include("inc/emailNaoInformado.inc");
                     unset($_GET);
                     }
                  }
               ?>
               <?php
               if(isset($_GET['naoAtivado'])){
                  if($_GET['naoAtivado'] == 1){
                     include("inc/naoAtivado.inc");
                     unset($_GET);
                     }
                  }
               ?>
               <div class="row">
                  <div class="form-group mx-auto  col-sm-12 col-md-12 col-lg-12">
                     <label for="senha">Senha:</label>
                     <input type="password" class="form-control" id="senha" placeholder="Sua senha" name="senha" required>
                  </div>
               </div>
               <?php
               if(isset($_GET['invalidacao'])){
                  if($_GET['invalidacao'] == 2){
                     include("inc/senhaNaoInformada.inc");
                     unset($_GET);
                  }
               }               
               ?>
               <?php
               if(isset($_GET['incorreto'])){
                  if($_GET['incorreto'] == 2){
                     include("inc/senhaIncorreta.inc");
                     unset($_GET);
                  }
               }               
               ?>
               <button type="submit" class="btn btn-primary col-xs-12 col-sm-12 col-md-12 col-lg-12">Entrar</button>
               <br><br>
               <div class="row">
                  <div class="form-group col-sm-12 col-md-12 col-lg-12">
                     <ul class="list-group">
                        <li class="list-group-item listaPrincipal">
                           <a href="cadastro.php?linktipo=2">Quero ser cliente do Serviço Fácil</a>
                        </li>
                        <br>
                        <li class="list-group-item listaPrincipal">
                           <a href="cadastro.php?linktipo=1">Sou profissional e quero me candidatar a prestar Serviços</a>
                        </li>

                     </ul>                                          
                  </div>
               </div>                             
            </div>
         </form>
      </div>
   </body>
</html>