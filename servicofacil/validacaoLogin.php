<?php
require_once("config.php");
session_start();
$email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
$senha = filter_input(INPUT_POST, "senha", FILTER_SANITIZE_STRING);

if(!isset($email) || !isset($senha)){   
   if(!isset($email)){
      header('location:index.php?invalidacao=1');
   }else{
      header('location:index.php?invalidacao=2');
   }   
}else{
   autenticaUsuario($email, $senha);   
}

function autenticaUsuario($email, $senha){
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   
   $statement = $conexao->query("SELECT * FROM usuarios WHERE email = '$email'");
   $result = $statement->fetch(PDO::FETCH_ASSOC);

   if($result){
      if($result['ativado'] == 0){
         header('location:index.php?naoAtivado=1');
      }else{
         $encriptSenha = base64_encode($senha);
         if($result['senha'] == $encriptSenha){
            $encriptMail = base64_encode($email);
            $tokenUsuario = $encriptMail;
            $_SESSION["usuario"] = $email;
            $_COOKIE["user_id"] = $result['usuario_id'];
            $_SESSION["creditos"] = $result['creditos'];
            $_SESSION["token"] = $tokenUsuario;
            $_SESSION["tipo"] = $result['tipo_id'];
            header('location:painel.php');
         }else{
            unset($_SESSION['usuario']);
            unset($_SESSION['token']);
            unset($_SESSION["tipo"]);
            unset($_SESSION["creditos"]);
            header('location:index.php?incorreto=2');
         }
      }      
   }else{
      unset($_SESSION['usuario']);
      unset($_SESSION['token']);
      unset($_SESSION["tipo"]);
      unset($_SESSION["creditos"]);
      header('location:cadastro.php?nao_encontrado=1&linktipo=2');
   }
}
?>