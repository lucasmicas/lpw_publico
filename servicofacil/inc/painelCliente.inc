<?php
$pagina = filter_input(INPUT_GET, 'pagina', FILTER_SANITIZE_NUMBER_INT);
if(isset($pagina)){
   $pagina_atual = $pagina;
}else{
   $pagina_atual = 1;
}
$maximo_por_pag = 5;
$inicio = $pagina_atual - 1;
$inicio = $inicio * $maximo_por_pag;

$servicos = getServicos($_SESSION["user_id"], $inicio, $maximo_por_pag);

function getServicos($id_usuario, $inicio, $maximo){
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $statement = $conexao->prepare("SELECT s.servico_id, s.data_criada, cat.nome_categoria, s.tipo_serv_id, t.nome_servico, s.status_id, st.nome_status, s.usuario_id, s.prestador_id, s.data_conclusao, s.pendencias, s.avaliacao, s.cancelador 
   FROM servico s 
   JOIN tipo_servico t ON t.tipo_serv_id = s.tipo_serv_id
   JOIN categoria cat ON cat.categoria_id = (SELECT categoria_id FROM tipo_servico WHERE tipo_serv_id = s.tipo_serv_id)
   JOIN status st ON st.status_id = s.status_id
   WHERE usuario_id = :id LIMIT :inicio, :maximo");
   $statement->bindParam(':id', $id_usuario, PDO::PARAM_INT);
   $statement->bindParam(':inicio', $inicio, PDO::PARAM_INT);
   $statement->bindParam(':maximo', $maximo, PDO::PARAM_INT);
   $statement->execute();   
   $result = $statement->fetchAll(PDO::FETCH_ASSOC);
   if(!$result){
      echo ("Erro ao buscar seus serviços. Podemos estar enfrentando problemas em nosso sistema temporariamente.");
   }else{
      return $result;
   }
}
?>

<html>
   <?php include("inc/heading.html"); ?>

   <body>
      <div class="container">

         <?php include("inc/avatar.inc"); ?>

         <h4>Minha lista de solicitações</h4>
         <a href="solicitarServico.php">Criar nova solicitação</a>

         <ul class="pagination justify-content-center">
            <?php
            $total_paginas = ceil(count($servicos) / $maximo_por_pag);

            if($pagina_atual > 1){
            ?>
            <li class="page-item"><a class="page-link" href="painel.php?pagina=<?=$pagina_atual - 1 ?>">Anterior</a></li>
            <?php
            }else{
            ?>
            <li class="page-item disabled"><a class="page-link" href="<?=$_COOKIE['home'] ?>painel.php?pagina=<?=$pagina_atual - 1 ?>">Anterior</a></li>
            <?php
            }
            for($i = 1; $i <= $total_paginas; $i++){
               if($i != $pagina_atual){
            ?>                  
            <li class="page-item">
               <?php                  
               }else{
               ?>
            <li class="page-item active">
               <?php
               }
               ?>                                             
               <a class="page-link" href="<?=$_COOKIE['home'] ?>painel.php?pagina=<?= $i ?>"><?= $i ?></a></li>
            <?php
            }
            if($pagina_atual < $total_paginas){
            ?>
            <li class="page-item">
               <?php
            }else{
               ?>
            <li class="page-item disabled">               
               <?php
            }
               ?>
               <a class="page-link" href="<?=$_COOKIE['home'] ?>painel.php?pagina=<?=$pagina_atual + 1 ?>">Próxima</a></li>
         </ul>

         <table class="table">
            <thead>
               <tr>
                  <th>Número</th>
                  <th>Data</th>
                  <th>Serviço</th>
                  <th>Status</th>
               </tr>
            </thead>
            <tbody>
               <?php               

   $EM_EXECUCAO = 1;
                  $PENDENTE = 2;
                  $CONCLUIDO = 3;
                  $CANCELADO = 4;
                  $NAO_INICIADO = 5;
                  //NUMERO, DATA, SERVICO/TIPO, STATUS/Link

                  foreach( $servicos as $servico ) {
                     if($servico['status_id'] == $EM_EXECUCAO){
               ?>
               <tr class="table-primary">
                  <td><?= $servico['servico_id'] ?></td>
                  <td><?= $servico['data_criada'] ?></td>
                  <td><?= $servico['nome_categoria'] . '<br>' . $servico['nome_servico'] ?></td>
                  <td><?= $servico['nome_status'] ?></td>
               </tr>
               <?php
                     }else if($servico['status_id'] == $PENDENTE){
               ?>
               <tr class="table-warning">
                  <td><?= $servico['servico_id'] ?></td>
                  <td><?= $servico['data_criada'] ?></td>
                  <td><?= $servico['nome_categoria'] . '<br>' . $servico['nome_servico'] ?></td>
                  <td><?= $servico['nome_status'] ?></td>
               </tr>
               <?php
                     }else if($servico['status_id'] == $CONCLUIDO){
               ?>
               <tr class="table-success">
                  <td><?= $servico['servico_id'] ?></td>
                  <td><?= $servico['data_criada'] ?></td>
                  <td><?= $servico['nome_categoria'] . '<br>' . $servico['nome_servico'] ?></td>
                  <?php
                  if($servico['avaliacao'] == null){
                  ?>
                  <td><?= $servico['nome_status'] ?><br><a href="<?= $_COOKIE['home'] ?>avaliar_prestador.php?id_servico="<?= $servico['servico_id'] ?>>Avaliar Prestador</a></td>
                  <?php
                  }else{
                  ?>
                  <td><?= $servico['nome_status'] ?><br>Avaliado: <?= $servico['avaliacao'] . "/5" ?></td>
                  <?php
                  }
                  ?>
               </tr>
               <?php
                     }else if($servico['status_id'] == $CANCELADO){
               ?>
               <tr class="table-danger">
                  <td><?= $servico['servico_id'] ?></td>
                  <td><?= $servico['data_criada'] ?></td>
                  <td><?= $servico['nome_categoria'] . '<br>' . $servico['nome_servico'] ?></td>
                  
                  <?php
                  if($servico['cancelador'] == $servico['prestador_id']){
                  ?>
                     <td><?= $servico['nome_status'] ?> pelo Prestador<br><?= $servico['data_conclusao'] ?></td>
                  <?php
                  }else{
                  ?>
                     <td><?= $servico['nome_status'] ?> pelo Cliente<br><?= $servico['data_conclusao'] ?></td>
                  <?php
                  }
                  ?>                     
               </tr>
               <?php
                     }else if($servico['status_id'] == $NAO_INICIADO){
               ?>
               <tr class="table-info">
                  <td><?= $servico['servico_id'] ?></td>
                  <td><?= $servico['data_criada'] ?></td>
                  <td><?= $servico['nome_categoria'] . '<br>' . $servico['nome_servico'] ?></td>
                  <td><?= $servico['nome_status'] . '<br>'?><a href="#" onclick="cancelar_servico(<?=$servico['servico_id']?>)">Cancelar</a></td>
               </tr>
               <?php
                     }
                  }
               ?>
            </tbody>
         </table>
      </div>
      <script>
         function cancelar_servico(id_servico){
            var jqxhr = $.post( "cancelar_servico.php", { id: id_servico})
            .done(function() {
               alert( "O serviço foi cancelado com sucesso!" );
            })
            .fail(function() {
               alert( "Erro no cancelamento." );
            });
         }
      </script>
   </body>
</html>