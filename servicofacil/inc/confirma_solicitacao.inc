<button id="modal_confirmaSolicitacao_trigger" type="button" hidden data-toggle="modal" data-target="#modal_confirmaSolicitacao"></button>

<div class="modal fade" id="modal_confirmaSolicitacao" role="dialog">
   <div class="modal-dialog">

      <div class="modal-content">
         <div class="modal-header">            
            <h4 class="modal-title">Solicitação Confirmada</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <p>Número de Solicitação: <span id="modal_num_solicitacao"></span></p>
            <p>Prestador: <span id="modal_prestador_escalado"></span></p>            
         </div>
         <div class="modal-footer">
            <p class="text-left"><a href="<?= $_COOKIE['home']?>painel.php">Ir para sua lista de solicitações</a></p>
         </div>
      </div>

   </div>
</div>