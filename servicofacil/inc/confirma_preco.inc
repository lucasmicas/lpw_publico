<button id="confirmaPreco_trigger" type="button" hidden data-toggle="modal" data-target="#modal_confirmaPreco"></button>

<div class="modal fade" id="modal_confirmaPreco" role="dialog">
   <div class="modal-dialog">
      
      <div class="modal-content">
         <div class="modal-header">            
            <h4 class="modal-title">Confirme o Preço do Serviço</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <p>Tipo de Serviço: <span id="modal_categoria_servico"></span></p>
            <p>Serviço: <span id="modal_tipoServico" value=""></span></p>
            <p>Preço: R$<span id="modal_preco"></span></p>
            <p>Seu crédito: R$<span id="modal_credito"></span></p>
            <p>Valor a ser cobrado: R$<span id="modal_cobrado"></span></p>
         </div>
         <div class="modal-footer">
            <button type="button" id="confirmaPrecoSolicitacao_trigger" class="btn btn-primary col-xs-12 col-sm-12 col-md-12 col-lg-12" data-dismiss="modal">Confirmar Serviço</button>
         </div>
      </div>

   </div>
</div>