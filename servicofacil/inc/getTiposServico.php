<?php
require_once("../config.php");
if(isset($_GET['cat'])){
   $dado = filter_input(INPUT_GET,'cat',FILTER_SANITIZE_NUMBER_INT);
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $statement = $conexao->query("SELECT tipo.nome_servico, tipo.tipo_serv_id, tipo.categoria_id FROM tipo_servico tipo WHERE tipo.categoria_id = $dado");
         
   $result = $statement->fetchAll();      

   foreach( $result as $tipo) {
      echo "<option value='" . $tipo['tipo_serv_id'] . "'>" . $tipo['nome_servico'] . "</option>";      
   }
}
?>