<?php
require_once("config.php");
if(isset($_GET['key'])){
   $key = filter_input(INPUT_GET,'key',FILTER_SANITIZE_STRING);
   $key = base64_decode($key);
   $conexao = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NOME.";charset=utf8",DB_USUARIO,DB_SENHA);
   $conexao->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

   if(!estaAtivado($key, $conexao)){
      if(ativaUsuario($key, $conexao)){
         include("inc/ativacaoBemSucedida.inc");
      }else{
         include("inc/ativacaoMalSucedida.inc");
      }
   }else{
      include("inc/contaJaAtivada.inc");
   }
}else{
   include("inc/verificarLinkAtivacao.inc");
};

function estaAtivado($email, $conexao){
   $statement = $conexao->query("SELECT ativado FROM usuarios WHERE email = '$email'");
   $result = $statement->fetch(PDO::FETCH_ASSOC);
   if($result['ativado'] == 0){
      return false;
   }else{
      return true;
   }		
}

function ativaUsuario($email, $conexao){
   if($statement = $conexao->query("UPDATE usuarios SET ativado = 1 WHERE email = '$email'")){
      return true;
   }else{
      return false;
   }
}
?>