function getPrecoTipoServico(id, callbackEscreve){
   var preco = 0;
   var xhr = new XMLHttpRequest();
   xhr.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         callbackEscreve(this.responseText);
      }
   };
   xhr.open("GET", "modulos/tiposervico/getPrecoServico.php?id_tipoServico=" + id, true);
   xhr.send();         
}